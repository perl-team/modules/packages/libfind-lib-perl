Source: libfind-lib-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 3.9.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libfind-lib-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libfind-lib-perl.git
Homepage: https://metacpan.org/release/Find-Lib

Package: libfind-lib-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends}
Multi-Arch: foreign
Description: Perl module to intelligently find libraries
 Find::Lib is a module aimed at finding and loading libraries in paths relative
 to the currently running script. It is particularly useful for test scripts,
 when there are test helper modules included in the distribution that are needed
 for testing but should not be installed on the system.
 .
 It simplifies the finding and loading these special libraries, which can be a
 tedious and error-prone process, especially when new modules are updated. This
 module is generally used to locate and load a bundled "bootstrapping" module
 which then loads the required dependencies.
